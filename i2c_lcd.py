# -*- coding: utf-8 -*-
# Original code found at:
# https://gist.github.com/DenisFromHR/cc863375a6e19dce359d

"""
Compiled, mashed and generally mutilated 2014-2015 by Denis Pleic
Made available under GNU GENERAL PUBLIC LICENSE

# Modified Python I2C library for Raspberry Pi
# as found on http://www.recantha.co.uk/blog/?p=4849
# Joined existing 'i2c_lib.py' and 'lcddriver.py' into a single library
# added bits and pieces from various sources
# By DenisFromHR (Denis Pleic)
# 2015-02-10, ver 0.1

"""
from machine import Pin, I2C

from time import sleep


class I2cDevice:
    def __init__(self, addr):
        self.addr = addr
        self.bus = I2C(scl=Pin(5), sda=Pin(4), freq=100000)

    def write_cmd(self, cmd):
        b = bytearray([cmd])
        self.bus.writeto(self.addr, b)
        sleep(0.0001)


# commands
LCD_CLEARDISPLAY = 0x01
LCD_RETURNHOME = 0x02
LCD_ENTRYMODESET = 0x04
LCD_DISPLAYCONTROL = 0x08
LCD_CURSORSHIFT = 0x10
LCD_FUNCTIONSET = 0x20
LCD_SETCGRAMADDR = 0x40
LCD_SETDDRAMADDR = 0x80

# flags for display entry mode
LCD_ENTRYRIGHT = 0x00
LCD_ENTRYLEFT = 0x02
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
LCD_DISPLAYON = 0x04
LCD_DISPLAYOFF = 0x00
LCD_CURSORON = 0x02
LCD_CURSOROFF = 0x00
LCD_BLINKON = 0x01
LCD_BLINKOFF = 0x00

# flags for display/cursor shift
LCD_DISPLAYMOVE = 0x08
LCD_CURSORMOVE = 0x00
LCD_MOVERIGHT = 0x04
LCD_MOVELEFT = 0x00

# flags for function set
LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00
LCD_2LINE = 0x08
LCD_1LINE = 0x00
LCD_5x10DOTS = 0x04
LCD_5x8DOTS = 0x00

# flags for backlight control
LCD_BACKLIGHT = 0x08
LCD_NOBACKLIGHT = 0x00

En = 0b00000100  # Enable bit
Rw = 0b00000010  # Read/Write bit
Rs = 0b00000001  # Register select bit


class Lcd:
    # initializes objects and lcd
    def __init__(self, address=0x3f):
        self._device = I2cDevice(address)
        self._backlight = LCD_BACKLIGHT

        self.write_cmd(0x03)
        self.write_cmd(0x03)
        self.write_cmd(0x03)
        self.write_cmd(0x02)

        self.write_cmd(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE)
        self.write_cmd(LCD_DISPLAYCONTROL | LCD_DISPLAYON)
        self.write_cmd(LCD_CLEARDISPLAY)
        self.write_cmd(LCD_ENTRYMODESET | LCD_ENTRYLEFT)
        sleep(0.2)

    # clocks EN to latch command
    def _strobe(self, data):
        self._device.write_cmd(data | En | self._backlight)
        sleep(.0005)
        self._device.write_cmd(((data & ~En) | self._backlight))
        sleep(.0001)

    def _write_four_bits(self, data):
        self._device.write_cmd(data | self._backlight)
        self._strobe(data)

    # write a command to lcd
    def write_cmd(self, cmd, mode=0):
        self._write_four_bits(mode | (cmd & 0xF0))
        self._write_four_bits(mode | ((cmd << 4) & 0xF0))

    # write a character to lcd (or character rom) 0x09: backlight | RS=DR<
    # works!
    def write(self, string):
        for char in string:
            self.write_cmd(ord(char), Rs)

    # put string function with optional char positioning
    def display_string(self, string, line=1, pos=0):
        if line == 2:
            pos += 0x40
        elif line == 3:
            pos += 0x14
        elif line == 4:
            pos += 0x54

        self.write_cmd(0x80 + pos)
        self.write(string)

    # clear lcd and set to home
    def clear(self):
        self.write_cmd(LCD_CLEARDISPLAY)
        self.write_cmd(LCD_RETURNHOME)

    # backlight on/off
    def backlight(self, enable):
        self._backlight = LCD_BACKLIGHT if enable else LCD_NOBACKLIGHT
        self._device.write_cmd(self._backlight)

    # add custom characters (0 - 7)
    def load_custom_chars(self, fontdata):
        self.write_cmd(0x40)
        for char in fontdata:
            for line in char:
                self.write_cmd(line, Rs)
