import sys

from i2c_lcd import Lcd

lcd = Lcd()

lcd.display_string(sys.implementation.name, 1)
lcd.display_string("%d.%d.%d" % sys.implementation.version, 1, 11)
lcd.display_string("----------------", 2)
